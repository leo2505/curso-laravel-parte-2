<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('index');

Auth::routes();

Route::get('/produtos', 'ControladorProduto@index')->name('produto');
Route::get('/categorias', 'ControladorCategoria@index')->name('categoria');
Route::get('/categorias/nova', 'ControladorCategoria@create')->name('novacategoria');
Route::get('/categorias/apagar/{id}', 'ControladorCategoria@destroy')->name('removercategoria');
Route::get('/categorias/editar/{id}', 'ControladorCategoria@edit')->name('editarcategoria');

Route::post('/categorias', 'ControladorCategoria@store')->name('post-categoria');
Route::post('/categorias/{id}', 'ControladorCategoria@update')->name('post-editar-categoria');