@extends('layout.app')

@section('body') 
    <div class="card border">
        <div class="card-body">
            <form action="/categorias" method="POST">
                @csrf
                <div class="form-group">
                    <label for="nomeCategoria">Nome da Categoria</label>
                    <input type="text" class="form-control" name="nomeCategoria" id="nomeCategoria" placeholder="Categoria">
                </div>
                <button type="submit" class="btn-primary btn-sn">Salvar</button>
                <!-- pode ser feito o button assim -->
                <button type="cancel" class="btn-danger btn-sn">Cancelar</button>
            </form>
        </div>
    </div>
@endsection