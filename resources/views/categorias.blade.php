@extends('layout.app')
@section('body')
    <h4>Página de categorias</h4>
    <div class="card border">
        <div class="card-body">
            @if(count($cat) > 0)
            <h5 class="card-title">Cadastro de Categorias</h5>
            <table class="table table-ordered table-hover">
                <thead>
                    <tr>
                        <th>Código</th>
                        <th>Nome da Categoria</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($cat as $item)
                    <tr>
                        <td>{{$item->id}}</td>
                        <td>{{$item->nome}}</td>
                        <td>
                            <a href="/categorias/editar/{{$item->id}}" class="btn btn-primary btn-sm">Editar</a>
                            <a href="/categorias/apagar/{{$item->id}}" class="btn btn-danger btn-sm">Apagar</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @else
                <p>Nenhum registro encontrado</p>
            @endif
        </div>
        <div class="card-footer">
            <a href="/categorias/nova" class="btn btn-sm btn-primary">Nova Categoria</a>
        </div>
    </div>
@endsection